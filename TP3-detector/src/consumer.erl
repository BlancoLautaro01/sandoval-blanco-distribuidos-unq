-module(consumer).
-export([start/1, stop/0]).

start(Producer) ->
    Consumer = spawn(fun() -> init(Producer) end),
    register(consumer, Consumer).

init(Producer) ->
    Monitor = monitor(process, Producer),
    Producer ! {hello, self()},
    consumer(0, Monitor).

consumer(ValorEsperado, Monitor) ->
    receive
        {ping, ValorEsperado} ->
            io:format("[OK] Valor esperado recibido correctamente: ~w~n", [ValorEsperado]),
            consumer(ValorEsperado + 1, Monitor);
        {ping, N} ->
            io:format("[WARNING] Valor incorrecto. Esperado:~w, recibido: ~w  ~n",[ValorEsperado, N]),
            consumer(N + 1, Monitor);
        {'DOWN', Monitor, process, Object, Info} ->
            io:format("~w died; ~w~n", [Object, Info]),
            consumer(ValorEsperado, Monitor);
        bye ->
            stop();
        stop ->
            stop()
    end.

stop() ->
    consumer ! kill.
