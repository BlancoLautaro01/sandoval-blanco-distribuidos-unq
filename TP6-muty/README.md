# TP 6: Muty, un lock de exclusión mutua distribuido

## Introducción

El trabajo consiste en un lock de exclusión mutua distribuido.
Existen `workers` que compiten por un recurso (en este caso una `gui`), y
cada uno posee un `lock`. Estos locks se comunican entre sí para ver si dejan
ingresar a su worker a la sección crítica. El primer lock que consigue todos los permisos
de sus pares será el que deje ingresar al worker a la sección crítica. Caso contrario,
deberá esperar a los locks de los que espera recibir `ok` le respondan.

El worker por su parte tiene un delay de espera para preguntarle a su lock,
y otro para trabajar dentro de la sección crítica.

Ese mecanismo de mensajeo entre locks se conoce como multicast. Es un término
que viene de las redes de computadoras en general, y significa que cada lock
se comunicará con un grupo de locks "interesados". A diferencia de broadcast,
en los que uno manda un mensaje a "todo el mundo", en este caso los mensajes se
mandan a un grupo específico con el que se entabla una comunicación.

### Estado de los Locks

Un lock puede estar en uno de tres estados: `open`, `waiting`, `held`. Esto
es simplemente modelado con distintas funciones en el módulo `lock` en este
trabajo.

En el estado `open` el lock está esperando que su worker le mande un mensaje
de `take`, con el que pasa al estado de `waiting` a preguntarle a todos los demás
locks que conoce si puede dejar a su worker entrar a la zona crítica, o bien, espera
un mensaje `request` para otorgarle su lock que no está siendo utilizado a otro que
esté necesitando entrar a la zona crítica.

Una vez que consigue todos los locks interesados pasa a estado `held`, en el que también
puede recibir requests de otros locks, y los añadirá a una lista de los locks
que están esperando confirmación por su parte.

Tanto en `held` como `waiting` el worker, o bien se puede cansar de esperar,
o bien termina su trabajo, pero en cualquier caso le avisa a su lock con un mensaje
de `release` que ya no le interesa tener el permiso. En este caso el lock le envía
un mensaje a todos los locks que tiene en la lista de "waiting", que son los que están
esperando su confirmación, de que ya no está interesado en el lock. Luego de esto 
siempre pasará a estado `open`.

Vamos a hacer tres implementaciones de locks, cada una dividida en un módulo diferente.
La interfaz será la misma para todos.

## Lock 1

La primera versión del lock es una versión naive, que simplemente evitará
que más de un worker entre a la sección crítica. Cuando el lock es iniciado, 
se le da un identificador único y un conjunto de locks pares. Ese identificador
no será utilizado en esta versión, pero como dijimos antes, para mantener
homogéneas las interfaces lo recibimos igual.

En esta versión el lock simplemente pasa de un estado a otro como describimos
en la introducción.

Los mensajes de request durante el estado de held son recibidos como describimos
anteriormente. Pero el enunciado igualmente pregunta si sería posible usar
la misma cola de mensajes de Erlang hasta liberarse el lock, y luego simplemente
ir despachando los mensajes encolados. La respuesta es sí, pero pensamos
que es interesante no encolar tantos mensajes de requests, para no llenar la
cola de mensajes.

Por otra parte también pregunta si en estado de `held` tiene sentido estar
esperando los mensajes de `ok`, pero no, no tiene sentido dado que nosotros
ya recibimos todos los `ok` que queríamos al entrar en ese estado particular.

Así que logramos implementar el módulo `lock1` sin mayores problemas.

## Lock 2

En esta otra versión, se utiliza el `Id` de lock recibido por parámetro
en su construcción.

Ese `Id` se utiliza principalmente en estado de `waiting`, dado que si recibimos
un request en ese momento, lo que hacemos es preguntar si el `Id` recibido
en el mensaje de request tiene más prioridad que el nuestro. De ser así, simplemente
le decimos `ok` al proceso que pide el request, pero en caso contrario lo hacemos
esperar, dado que nosotros somos más prioritarios. Si los `Id` son iguales decidimos 
hacerlo esperar también, dado que sino
dos procesos podrían entrar a la zona crítica en simultáneo.

Efectivamente chequeamos que este mecanismo funciona, pero tiene la desventaja
de que un lock con prioridad alta podría estar ganando más veces el permiso,
los de menos prioridad tienen muchas menos probabilidades de conseguirlo.

¿Podemos garantizar que tenemos un solo proceso en la sección crítica en todo momento?
Creemos que sí, dado que al competir por el recurso el mecanismo utilizado es similar
al anterior, y si hay un proceso en la zona crítica, este no va a decirle `ok`
a nadie.

No notamos grandes diferencias de eficiencia con respecto a la versión anterior,
pero sí creemos que con grandes cantidades de locks, la competencia por el recurso
debería terminar antes, dado que si los `Id` son todos diferentes, uno será más
prioritario que el resto, e ingresará a la sección de crítica con mayor anticipación.
Una desventaja clara es que, los de menor prioridad, dependiendo de los tiempos de
`Sleep` y `Work` de los procesos, podrían nunca acceder a la sección crítica y caer
siempre en estado de no interesarle más la sección crítica (definido como una
espera que será un deadlock).

Tuvimos un inconveniente en que dos workers accedían a la vez a la sección crítica.
Identificamos gracias a la ayuda del profesor un caso borde donde, un lock de
baja prioridad conseguía el `ok` de otro de más alta, y mientras esperaba,
ese otro lo solicitaba, entonces para solucionarlo, el de baja prioridad debe volver
a solicitarle el lock asegurándose de que el de mayor prioridad no entre antes que él. 
A su vez, nos faltaba el parámetro de id al recibir mensajes `request`, por lo
que no entraba por el caso que debía. Luego pudimos finalizarlo correctamente.

## Lock 3

Lo que se aplica en esta versión es una mejora sobre la anterior. Cada lock
ahora tendrá un reloj de Lamport (que nos trajimos del trabajo anterior, `Loggy`).
Así, al enviar un request un lock aumenta su reloj, y al recibirlos lo actualiza.
El reloj mayor será el que se guarde en este caso.

Cuando recibimos un request además tenemos que decidir si nuestro pedido tiene
un tiempo menor al del otro. De eser así, lo hacemos esperar. Caso contrario, cuando
es menor el del otro, le damos un `ok`. Si llegan a ser iguales, desempatamos por
`Id`, igual que en el módulo de `lock2`.

*¿Puede darse la situación en que un worker no se le da prioridad al lock a pesar
de que envió el request a su lock con un tiempo lógico anterior al worker que lo
consiguió?*. Es posible, dado que otro lock con tiempo mayor obtumo primero
los permisos que deseaba, y el anterior no llegó a consultar a todos los que debía
a tiempo.

Por otra parte, la eficiencia obtenida fue similar a la anterior, dado que decidimos tener
relojes de Lamport que son simplemente un número. Con vectores de relojes creemos
que la eficiencia podría ser peor, dado que debe recorrer el vector de locks
para decidir si un reloj es menor al otro.

Teníamos casos donde se volvía inconsistente la implementación, pero este problema
se daba porque estabamos actualizando mal la forma de incrementar el reloj lamport
de los locks. Luego integramos el desempate por id, y pudimos terminarlo sin
problemas.

## Pruebas

Tomamos el módulo `test` provisto por la cátedra que representa 4 workers
compitiendo por el lock con una interfaz gráfica, provista en el módulo `gui`,
haciendo más visible y sencilla la utilización de los locks.

Cuando un worker está trabajando, puede verse que su ventana aparece con fondo
rojo. A su vez, la ventana azul indica que el worker abandona su trabajo,
ya sea porque se cansó o porque ya decide que terminó su turno. En amarillo
aparecen aquellos que se encuentran esperando a entrar, y no están en ninguno
de esos dos casos.

Así, pudimos observar que en las distintas implementaciones una sola ventana
aparece en rojo en todo momento. Y eso también nos permitió observar si
la competencia por el recurso era "fair", o un proceso en especial tenía
mayores probabilidades de conseguirlo, dado que aparecía más veces en rojo
que el resto.

## Conclusiones

Aprendimos sobre comunicación multicast, y un mecanismo de lock distribuido.
Hasta el momento siempre pensábamos que un lock debía estar siempre centralizado,
pero este trabajo demostró que puede realizarse de forma distribuida, manteniendo
varios recaudos, y con trade-offs en las implementaciones. Creemos que la última
implementación que utiliza relojes de Lamport es la más adecuada, dado que
prioriza al que primero comenzó a pedir los permisos. La segunda nos parece
la menos adecuada, dado que la prioridad sobre procesos puede llegar a ser
poco "fair", justamente con los procesos de prioridad baja, y, En este caso,
no queda claro por qué un worker tendría menos prioridad que otro para entrar
a la zona crítica.

Hicimos varias pruebas. Al principio nos costó detectar algunos casos borde,
como los que un lock manda un request al mismo tiempo que otro también lo manda
a este. En ese caso se produce un **deadlock**, pero es resuelto por el worker,
dado que este manda un relase cuando ya considera que esperó el tiempo suficiente
por el recurso.s
