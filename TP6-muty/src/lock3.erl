-module(lock3).

-export([start/1]).

start(Id) ->
    spawn(fun() -> init(Id) end).

init(Id) ->
    receive
        {peers, Peers} ->
            open(Peers, time:zero(), Id);
        stop ->
            ok
    end.

open(Nodes, Time, Id) ->
    receive
        {take, Master} ->
            NewTime = Time + 1,
            Refs = requests(Nodes, NewTime, Id),
            wait(Nodes, NewTime, Master, Refs, [], Id);
        {request, From, Ref, RefTime} ->
            From ! {ok, Ref},
            open(Nodes, time:merge(Time, RefTime), Id);
        stop ->
            ok
    end.

requests(Nodes, Time, Id) ->
    lists:map(
        fun(P) ->
            R = make_ref(),
            P ! {request, self(), R, Time, Id},
            R
        end,
        Nodes
    ).

wait(Nodes, Time, Master, [], Waiting, Id) ->
    Master ! taken,
    held(Nodes, Time, Waiting, Id);
wait(Nodes, Time, Master, Refs, Waiting, Id) ->
    receive
        {request, From, Ref, RefTime, RefId} when Time == RefTime ->
            case Id =< RefId of
                true ->
                    wait(Nodes, Time, Master, Refs, [{From, Ref} | Waiting], Id);
                false ->
                    R = make_ref(),
                    From ! {ok, Ref},
                    From ! {request, self(), R, Id},
                    wait(Nodes, Time, Master, [R | Refs], Waiting, Id)
            end;
        {request, From, Ref, RefTime, _RefId} ->
            NewTime = time:merge(Time, RefTime),
            case time:leq(Time, RefTime) of
                true ->
                    wait(Nodes, NewTime, Master, Refs, [{From, Ref} | Waiting], Id);
                false ->
                    From ! {ok, Ref},
                    wait(Nodes, NewTime, Master, Refs, Waiting, Id)
            end;
        {ok, Ref} ->
            Refs2 = lists:delete(Ref, Refs),
            wait(Nodes, Time, Master, Refs2, Waiting, Id);
        release ->
            ok(Waiting),
            open(Nodes, Time, Id)
    end.

ok(Waiting) ->
    lists:foreach(fun({F, R}) -> F ! {ok, R} end, Waiting).

held(Nodes, Time, Waiting, Id) ->
    receive
        {request, From, Ref, RefTime, _RefId} ->
            NewTime = time:merge(Time, RefTime),
            held(Nodes, NewTime, [{From, Ref} | Waiting], Id);
        release ->
            ok(Waiting),
            open(Nodes, Time, Id)
    end.
