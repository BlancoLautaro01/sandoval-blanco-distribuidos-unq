-module(lock2).

-export([start/1]).

start(Id) ->
    spawn(fun() -> init(Id) end).

init(Id) ->
    receive
        {peers, Peers} ->
            open(Peers, Id);
        stop ->
            ok
    end.

open(Nodes, Id) ->
    receive
        {take, Master} ->
            Refs = requests(Nodes, Id),
            wait(Nodes, Id, Master, Refs, []);
        {request, From, Ref, _ } ->
            From ! {ok, Ref},
            open(Nodes, Id);
        stop ->
            ok
    end.

requests(Nodes, Id) ->
    lists:map(
        fun(P) ->
            R = make_ref(),
            P ! {request, self(), R, Id},
            R
        end,
        Nodes
    ).

wait(Nodes, Id, Master, [], Waiting) ->
    Master ! taken,
    held(Nodes, Id, Waiting);
wait(Nodes, Id, Master, Refs, Waiting) ->
    receive
        {request, From, Ref, PeerId} ->
            if
                Id > PeerId ->
                    R = make_ref(),
                    From ! {ok, Ref},
                    From ! {request, self(), R, Id},
                    wait(Nodes, Id, Master, [R | Refs], Waiting);
                true ->
                    wait(Nodes, Id, Master, Refs, [{From, Ref} | Waiting])
            end;
        {ok, Ref} ->
            Refs2 = lists:delete(Ref, Refs),
            wait(Nodes, Id, Master, Refs2, Waiting);
        release ->
            ok(Waiting),
            open(Nodes, Id)
    end.

ok(Waiting) ->
    lists:foreach(fun({F, R}) -> F ! {ok, R} end, Waiting).

held(Nodes, Id, Waiting) ->
    receive
        {request, From, Ref, _ } ->
            held(Nodes, Id, [{From, Ref} | Waiting]);
        release ->
            ok(Waiting),
            open(Nodes, Id)
    end.
