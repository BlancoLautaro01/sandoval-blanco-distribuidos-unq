# TP 7: Toty, multicast con orden total

## Introducción

En esta actividad implementamos multicast entre varios workers que
desean mantener un estado interno (un color RGB).  Ese estado es el
color de una ventana que cada uno posee, y lo que hace cada worker 
es enviar un número a los demás, que deben sumar de cierta forma a ese estado.

Para sumar utilizamos esta función, donde `N` es el número a sumar,
y el estado es un valor RGB:

```
addN({ R, G, B }, N) -> { G, B, (R+N) rem 256 }.
```

Cada worker entonces conoce un proceso `multicast` que le permite
enviar el mensaje deseado a los demás workers.

La pantalla de cada worker empieza en negro, y lo que queremos confirmar
es si todos los workers reciben los mensajes en el orden correcto, pasando
todos por exactamente los mismos colores. Si esto no se produce, hay workers
que empezarán a diferir sus colores, dado que la función depende del orden
en que suma los números que recibe.

## Primera Implementación: multicast básico

Para la primera implementación, basados en el enunciado del trabajo,
lo que hicimos fue pasar un tiempo de `Sleep` al worker y un tiempo de `Jitter`
al proceso de multicast. El primero nos sirve para que el worker espere
antes de mandar su mensaje, y lo segundo para que una vez recibido el mensaje
del worker, el multicast espere para enviarlo a los demás partes que conoce.

También decidimos que el proceso de multicast reciba a su master en un primer
mensaje, y luego sus nodos pares en otro. A partir de ahí pasa al estado `server`
en el que espera tanto mensajes de su worker, como mensajes de sus pares, que
entrega al worker apenas los recibe.

Creemos que mientras más rápido se envíen los mensajes, más rápido 
empiezan a diferir los workers los colores de su estado interno, dado que
no se garantiza un orden total en la entrega de los mensajes. No toma mucho 
tiempo hasta que esto sucede.

Es difícil depurar los procesos para saber en qué punto comenzaron a diferir.
Lo que hicimos fue imprimir los números recibidos en el orden en que
llegaron, y también los colores resultantes de cada worker. Ahí observamos
que en determinado momento un worker recibía un número a la vez que otro worker
estaba recibiendo otro número distinto, y ahí observamos que los colores de
ambos workers comenzaron a diferir desde ese punto.

## Segunda Implementación: con orden total

Completamos el código haciendo algunos cambios a la representación propuesta.
Por ejemplo, en el valor de secuencia sólo mantenemos un número, dado que 
no hizo falta que utilicemos el `Id` que lo acompañaba en el enunciado.

Por otra parte, los valores de la `Queue` poseen un campo más, que es un atom
que varía entre `proposal` y `agreed`. Cuando se encola por primera vez,
se asigna el primer estado, y una vez que se obtiene el concenso pasa a ser
`agreed`, lo que nos permite filtrar los que ya podemos enviar a nuestro
worker, aunque siempre ordenados por el valor consensuado.

Realizamos pruebas nuevamente usando el multicast de orden total. Comenzamos
probando con 4 workers con sus respectivos multicasters. Notamos
que los colores llegan en el orden correcto para todos, por lo que comprobamos
que para este caso el multicaster implementado funcionó.

Luego hicimos pruebas con 8, 12 y 20 nodos. Jugamos con distintos tiempos de
sleep y jitter, variando los tiempos entre 100 ms y 5000 ms, para ambas variables.
Notamos que es posible que un mensaje de un worker empiece a acordarse antes
que otro, pero este último puede ser entregado antes, si consigue el acuerdo
primero. Es decir, hay dos multicasters que consiguen acuerdo casi al mismo tiempo,
y entran a repartir el mensaje de agreed, pero en distinto orden a los nodos
que conocen. Es posible que varias ventanas se empiecen a pintar del color
que uno de los multicasters propuso, y otra comience a pintarse en otro de esos
colores. Cuando la red sea lo suficientemente grande, es posible que esto empiece a pasar,
dado que existe una demora en la red, y distintos nodos podrían proponer el mismo
orden para distintos mensajes. Hemos detectado casos en los que con una red de 4 nodos, 
con tiempos muy chicos, este efecto sucede.

Como los workers siempre esperan a recibir su mensaje, la congestión
de la red disminuye, y un worker manda tantos mensajes por segundo, como lo que
tarda la red en acordar que su mensaje fue acordado, y este lo recibe.

## Conclusiones

Hemos implementado otra actividad orientada al multicast de mensajes, pero en
este caso los multicasters deben ponerse de acuerdo para asignar un orden
total a los mensajes, y así imprimirlos todos en un mismo orden, dado que
la operación que realizan los workers no es conmutativa, y el orden por ende
importa (a diferencia de muty que eran asincrónicos).

Así, los procesos multicast esperan el envío de mensaje que proponen sus respectivos
workers, para luego pasar a mandar un request a sus pares, donde luego se esperan
propuestas de orden de cada uno. Cuando se reciben todas las propuestas es donde
se llega al acuerdo buscado.

Notamos que esta técnica funciona en muchos casos, y detectamos algunos otros en los
que no. En algunas situaciones pudimos detectar el por qué, y en otras se nos
hizo más difícil, sobre todo cuando acortábamos los tiempos de sleep y jitters,
variables que manejaban las demoras al enviar y recibir mensajes.

Finalmente, también notamos que fue la actividad que menos ayuda recibimos
por parte del enunciado que teníamos que seguir. Muchas cosas las tuvimos que
pensar e incluso tratar de imaginar, lo que nos llevó a cambiar algunas de estas,
tratando de encontrarles algún funcionamiento útil. Creemos que llegamos
a una solución, aunque podría darse el caso que algunas de las fallas que encontramos
se deban a errores en nuestro código, por lo que creemos que es el trabajo
con más complejidad que realizamos hasta el momento, tanto por la
ayuda justa del enunciado, como por la carga conceptual para implementar y
resolver el trabajo.
