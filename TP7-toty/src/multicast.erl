-module(multicast).
-export([start/2]).

start(_, Jitter) ->
    spawn(fun() -> waitForPeers(Jitter) end).

waitForPeers(Jitter) ->
    receive
        { peers, Nodes } -> waitForMaster(Jitter, Nodes);
    stop ->
        ok
    end.

waitForMaster(Jitter, Nodes) ->
    receive
        { master, Master } ->
            Queue = [],
            Cast  = [],
            Next  = 0,
            server(Master, Next, Nodes, Cast, Queue, Jitter);
    stop ->
        ok
    end.

server(Master, Next, Nodes, Cast, Queue, Jitter) ->
    receive
        {send, Msg} ->
            Ref = make_ref(),
            request(self(), Ref, Msg, Nodes),
            Cast2 = cast(Ref, length(Nodes)+1, Cast),
            server(Master, Next, Nodes, Cast2, Queue, Jitter);
        {request, From, Ref, Msg} ->
            From ! { proposal, Ref, Next },
            Queue2 = insert(Ref, Next, Msg, Queue),
            Next2 = increment(Next),
            server(Master, Next2, Nodes, Cast, Queue2, Jitter);
        {proposal, Ref, Proposal} ->
            case proposal(Ref, Proposal, Cast) of
                {agreed, Seq, Cast2} ->
                    agree(Ref, Seq, Nodes),
                    server(Master, Next, Nodes, Cast2, Queue, Jitter);
                Cast2 ->
                    server(Master, Next, Nodes, Cast2, Queue, Jitter)
            end;
        { agreed, Ref, Seq } ->
            Max = lists:max(Seq),
            Updated = update(Ref, Max, Queue),
            { Agreed, Queue2 } = agreed(Max, Updated),
            deliver(Master, Agreed, Jitter),
            Next2 = increment(Max, Next),
            server(Master, Next2, Nodes, Cast, Queue2, Jitter)
    end.

request(From, Ref, Msg, Nodes) ->
    lists:foreach(fun(N) ->
        N ! { request, From, Ref, Msg }
    end, [ From | Nodes ]).

agree(Ref, Seq, Nodes) ->
    lists:foreach(fun(N) ->
        N ! { agreed, Ref, Seq }
    end, [ self() | Nodes ]).

cast(Ref, L, Cast) ->
    [ { Ref, L, [] } | Cast ].

increment(N) ->
    N+1.

increment(M, N) ->
    max(M, N)+1.

proposal(Ref, Proposal, Cast) ->
    case lists:keyfind(Ref, 1, Cast) of
        { Ref, 1,  Seq }  -> { agreed, [ Proposal | Seq ], lists:keydelete(Ref, 1, Cast) };
        { Ref, N,  Seq }  -> lists:keyreplace(Ref, 1, Cast, { Ref, N-1, [ Proposal | Seq ] });
        false -> error_logger:error_msg("Imposible Proposal!")
    end.

insert(Ref, Next, Msg, Queue) ->
    lists:keysort(3, [ { proposal, Ref, Next, Msg } | Queue ] ).

update(Ref, M, Queue) ->
    case lists:keyfind(Ref, 2, Queue) of
        { _, Ref, _, Msg } -> 
            lists:keysort(3, 
                lists:keyreplace(Ref, 2, Queue, { agreed, Ref, M, Msg })
            );
        false -> 
            error_logger:error_msg("Imposible Update!")
    end.

agreed(_, Queue) ->
    lists:splitwith(fun(X) ->
        case X of
            { agreed, _, _, _ } -> true;
            _                   -> false
        end
    end, Queue).

deliver(Master, Agreed, Jitter) ->
    timer:sleep(Jitter),
    lists:foreach(fun({ _, _, _, Msg }) ->
        Master ! { response, Msg }
    end, Agreed).