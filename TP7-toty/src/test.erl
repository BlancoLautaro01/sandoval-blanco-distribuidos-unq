-module(test).
-export([run/3]).

run(Size, Jitter, Sleep) ->
	% Generamos los multicast
	Data = lists:map(fun(N) ->
		Multicaster  = multicast:start("MultiCast " ++ integer_to_list(N), Jitter),
		Name  = "Worker " ++ integer_to_list(N),
		Seed  = N,	
		{ Name, Multicaster, Seed }
	end, lists:seq(1, Size)),

	% Tomamos los multicasters
	Multicasters = lists:map(fun({ _, Multicaster, _ }) -> 
		Multicaster
	end, Data),

	% Le enviamos a cada multicast sus pares
	lists:foreach(fun(Multicast) -> 
		Multicast ! { peers, lists:delete(Multicast, Multicasters) }
	end, Multicasters),

	% Levantamos los workers y le asignamos sus respectivos multicasters
	lists:foreach(fun({ Name, Multicast, Seed }) -> 
		worker:start(Name, Multicast, Seed, Sleep)
	end, Data).