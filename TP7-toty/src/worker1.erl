-module(worker1).
-export([start/4, stop/1]).

start(Name, Manager, Sleep, Jitter) ->
    register(Name, spawn(fun() -> init(Name, Manager, {0, 0, 0}, Sleep, Jitter) end)).

init(Name, Manager, State, Sleep, Jitter) ->
    Gui = gui:start(Name),
    Manager ! {subscribe, Name, Jitter},
    receive
        {cast, MultiCast} ->
            worker(Name, Gui, State, Sleep, MultiCast)
    end.

stop(Worker) ->
  Worker ! stop.

worker(Name, Gui, State, Sleep, Cast) ->
    receive
        {msg, Msg} ->
            io:format("[~w] me llego: ~p~n", [Name, Msg]),
            NTuple = color_change(Msg, State),
            io:format("[~w] tengo el color: ~p~n", [Name, NTuple]),
            Gui ! {color, NTuple},
            worker(Name, Gui, NTuple, Sleep, Cast);
        stop ->
            Gui ! stop
    after Sleep ->
        Message = rand:uniform(20),
        io:format("[~w] envio: ~p~n", [Name, Message]),
        Cast ! {send, Message},
        receiveMsg(Name, Message, Gui, State, Sleep, Cast)
    end.

receiveMsg(Name, Message, Gui, State, Sleep, Cast) ->
    receive
        {msg, Msg} ->
            io:format("[~w] me llego: ~p~n", [Name, Msg]),
            NTuple = color_change(Msg, State),
            io:format("[~w] tengo el color: ~p~n", [Name, NTuple]),
            Gui ! {color, NTuple},
            case Msg == Message of
                true -> worker(Name, Gui, NTuple, Sleep, Cast);
                false -> receiveMsg(Name, Message, Gui, NTuple, Sleep, Cast)
            end;
        Bla ->
            io:format("llego otra cosa: ~w~n", [Bla])
    end.

color_change(N, {R, G, B}) ->
    {G, B, ((R + N) rem 256)}.
