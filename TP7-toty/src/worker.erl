-module(worker).
-export([start/4]).

start(Name, Multicast, Seed, Sleep) ->
    spawn(fun() -> init(Name, Multicast, Seed, Sleep) end).
    
init(Name, Multicast, Seed, Sleep) ->
    Multicast ! { master, self() },
    Gui = spawn(gui, init, [Name]),
    rand:seed(default, Seed),
    State = { 0, 0, 0 },
    worker(Multicast, State, Sleep, Gui),
    Gui ! stop.

worker(Multicast, State, Sleep, Gui) ->
    Wait = rand:uniform(Sleep),
    receive
        stop ->
            stop
    after Wait ->
        N = rand:uniform(20),
        Multicast ! { send, N },
        NewState = paint(N, State, Gui),
        worker(Multicast, NewState, Sleep, Gui)
    end.

paint(N, State, Gui) ->
    receive
        { response, N } -> 
            NewState = addN(State, N),
            Gui ! { paint, NewState },
            NewState;
        { response, N2 } -> 
            NewState = addN(State, N2),
            Gui ! { paint, NewState },
            paint(N, NewState, Gui)
    end.

addN({ R, G, B }, N) -> { G, B, (R+N) rem 256 }.