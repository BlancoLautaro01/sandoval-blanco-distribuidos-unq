-module(multicast1).
-export([start/2, stop/0]).

start(Jitter, Workers) ->
    register(cast, spawn(fun() -> init_workers(Jitter, Workers) end)).

init_workers(Jitter, Workers) ->
    lists:foreach(
        fun(Worker) ->
            Worker ! {cast, self()}
        end, Workers),
    multicast(Jitter, Workers).

multicast(Jitter, Workers) ->
    receive
        {send, RGB} ->
            send(Workers, RGB, Jitter),
            multicast(Jitter, Workers);
        stop ->
            ok
    end.

send(Workers, RGB, Jitter) ->
    timer:sleep(Jitter),
    lists:foreach(
        fun(Worker) ->
            Worker ! {update, RGB}
        end, Workers).

stop() ->
    cast ! stop.