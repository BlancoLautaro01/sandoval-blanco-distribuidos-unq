# TP4 Opty: control concurrencia optimista

El control de bloqueo optimista o control de concurrencia optmista nos permite reducir el nivel de aislamiento que se utiliza en una aplicación.
Este tipo de control permite que varios usuarios intenten actualizar el mismo registro sin informar a los usuarios de que otros también están intentando actualizar el registro.

Un bloqueo optimista es el que evita la sobrecarga del bloque de un registro mientras dure la acción.

### Performance

#### 1. ¿Performa?
La ejecución de cada instrucción se realiza de forma rápida, solo en el caso del Commit tarda un poco más, y esto es debido a que se debe validar el Commit y el validador es único.

Para corroborar esto, optamos por crear un benchmark que recibe un numero de cliente y operaciones que se desea que el cliente realice. Se generan cocurrentemente la cantidad de clientes pedidas y estos ejecutan las N operaciones alternando entre la eleccion aleatoria de leer o escribir, al terminar las N operaciones, realizan un commit e informan el resultado a el proceso principal para que recolecte la información de los clientes.


#### 2. Pruebas con el benchmark
Hemos realizado varias pruebas con el benchmark que generamos, y obtuvimos los siguientes resultados:

```
100 clientes, 1 operaciones cada uno, 100 entradas
  Total clients: 100
  Total entries: 100
###########################################
  Total reads: 50
  Total writes: 50
  Total commits: 100
  Total commits fails: 17
###########################################
  Commits failure rate: 0.17
```

```
100 clientes, 5 operaciones cada uno, 100 entradas
  Total clients: 100
  Total entries: 100
###########################################
  Total reads: 260
  Total writes: 240
  Total commits: 100
  Total commits fails: 69
###########################################
  Commits failure rate: 0.69
```

```
100 clientes, 5 operaciones cada uno, 1000 entradas
  Total clients: 10
  Total entries: 1000
###########################################
  Total reads: 6
  Total writes: 4
  Total commits: 10
  Total commits fails: 0
###########################################
  Commits failure rate: 0.0
```

```
10 clientes, 2 operaciones cada uno, 2 entradas
Total clients: 10
  Total entries: 2
###########################################
  Total reads: 12
  Total writes: 8
  Total commits: 10
  Total commits fails: 7
###########################################
  Commits failure rate: 0.7
```

#### 3. ¿Cuales son las limitaciones en el número de transacciones concurrentes y la tasa de éxito?
Al realizar las distintas pruebas notamos que la tasa de éxito por commits se mide de forma proporcionalmente indirecta:
- Por un lado, la cantidad de clientes multiplicada por la cantidad de operaciones que realizan(cantidad de transacciones).
- Por el otro, la cantidad de entradas (tamaño del Store).
Entonces:
- Mientras más grande sea el primer número y menor el segundo, aumentará la tasa de fallos.
- Mientras más chico sea el primer número y mayor el segundo, aumentará la tasa de éxito.

#### 4. ¿Algo más?
En el store no se soportan muchos registros, el máximo que puede soportar es dependiendete de la memoria del equipo no llega a 100,000.
Aun asi, con la ayuda distribuida de nuestro test, pudimos corroborar casos donde el numero entradas supera el dicho anteriormente.

#### 5. ¿Es realista la implementación del store que tenemos?
La implementación no es del todo realista. Porque el validador es un cuello de botella,
dado que es un único proceso validando los commits de todos los usuarios. Más
clientes hacen commit más tarda en responderle a cada uno.
El handler corre en el cliente, y se le pasan el PID del servidor y el validador. Si se desea
distribuir el trabajo deberían crearse varios servidores con sus respectivos validadores,
los cuales seguramente posean una copia del store para operar de forma más eficiente.
Uno de los pro es que el proceso del handler al linkearse con el cliente muere si el cliente
muere. Pero contrae algo en contra que es que el servidor no tiene control sobre sus handlers, por lo que
es posible abrir un número descontrolado de handlers, o incluso que un mismo cliente abra
todos los handlers que desee.

#### 6. ¿Qué rápido podemos operar sobre el store?
Es rápido porque no depende de otro módulo, solo cuando se le solicita añade o toma de la lista.

#### 7. ¿Qué sucede si hacemos esto en una red de Erlang distribuida, qué es lo que se copia cuando el handler de transacciones arranca? ¿Dónde corre el handler?
Cuando el handler de transacciones arranca, se copia el Client, Validator y el Store. Y el handler se genera en un proceso nuevo del lado del Cliente.

#### 9. ¿Cuales son los pros y contras de la estrategia de implementación?

##### Pros
- El uso del Handler ayuda a realizar todas las operaciones: read, write
- El store que es donde almacena los datos, hace los escrituras directamente al archivo y también toma un dato en caso de necesitarlo.
- El uso del control de concurrencia optimista hace un mejor rendimiento ya que asume múltiples transacciones y estas se puedan completar frecuentemente sin interferir entre sí.
- El Store posee sus claves dentro de procesos, por lo que la caída de una entrada no afectaría el trabajo del resto que podría seguir con normalidad y, al ser procesos, podría programarse para que fueran tolerantes a fallos.

##### Contra
- El Validator es único en el servidor, por lo que genera que las validaciones al commitear sean lentas(1000 commits, 1000 validaciones secuenciales).
- No es muy óptimo con transacciones largas (agregando un delay en los clientes de benchmark reduciría considerablemente la tasa de éxito).
