-module(client).
-export([open/1, read/2, write/3, commit/1, abort/1]).

open(Server) ->
    Server ! {open, self()},
    receive
        {transaction, Validator, Store} ->
            handler:start(self(), Validator, Store)
    end.

read(Handler, Key) ->
    Ref = make_ref(),
    Handler ! {read, Ref, Key},
    receive
        {Ref, Value} -> Value
    end.

write(Handler, Key, Value) ->
    Handler ! {write, Key, Value}.

commit(Handler) ->
    Ref = make_ref(),
    Handler ! {commit, Ref},
    receive
        {Ref, ok} -> ok;
        {Ref, abort} -> abort
    end.

abort(Handler) -> Handler ! abort.
