-module(testOpty).
-export([bench/3]).

bench(Clients, Ops, Entries) ->
    Start = erlang:system_time(micro_seconds),
    server:start(Entries),
    runClients(Clients, Ops, Entries),
    {CWrites, CReads, CCommits, CFails} = waitClients(Clients, 0, 0, 0, 0),
    Finish = erlang:system_time(micro_seconds),
    Totaltime = (Finish - Start) / 1000000,
    server:stop(server),

    io:format("  Total clients: ~w~n", [Clients]),
    io:format("  Total entries: ~w~n", [Entries]),
    io:format("###########################################~n"),
    io:format("  Total reads: ~w~n", [CReads]),
    io:format("  Total writes: ~w~n", [CWrites]),
    io:format("  Total commits: ~w~n", [CCommits]),
    io:format("  Total commits fails: ~w~n", [CFails]),
    io:format("###########################################~n"),
    io:format("  Commits failure rate: ~w~n", [CFails / CCommits]),
    io:format("###########################################~n"),
    io:format("  Total test time: ~w~n", [Totaltime]).

runClients(Clients, Ops, Entries) ->
    TestPid = self(),
    case Clients of
        0 ->
            ok;
        _ ->
            spawn_link(fun() ->
                Handler = client:open(server),
                operate(Handler, Ops, Entries, TestPid, 0, 0)
            end),
            runClients(Clients - 1, Ops, Entries)
    end.

operate(Handler, Ops, Entries, TestPid, CReads, CWrites) ->
    case Ops of
        0 ->
            Status = client:commit(Handler),
            TestPid ! {Status, CReads, CWrites};
        _ ->
            Op = rand:uniform(2),
            Entry = rand:uniform(Entries),
            case Op of
                1 ->
                    client:read(Handler, Entry),
                    operate(Handler, Ops - 1, Entries, TestPid, CReads + 1, CWrites);
                2 ->
                    client:write(Handler, Entry, rand:uniform(9999)),
                    operate(Handler, Ops - 1, Entries, TestPid, CReads, CWrites + 1)
            end
    end.

waitClients(Clients, CWrites, CReads, CCommits, CFails) ->
    case Clients of
        0 ->
            {CWrites, CReads, CCommits, CFails};
        _ ->
            receive
                {Status, Reads, Writes} ->
                    case Status of
                        abort ->
                            waitClients(
                                Clients - 1,
                                CWrites + Writes,
                                CReads + Reads,
                                CCommits + 1,
                                CFails + 1
                            );
                        ok ->
                            waitClients(
                                Clients - 1,
                                CWrites + Writes,
                                CReads + Reads,
                                CCommits + 1,
                                CFails
                            )
                    end
            end
    end.
