-module(time).
-export([zero/1, inc/2, merge/2, leq/2, clock/1, update/3, safe/2]).

%%%%% RELOJES VECTORIALES %%%%%%%%
zero(Nodes) ->
    lists:map(fun(N) -> { N, 0 } end, Nodes).

inc(Name, T) ->
	{ Name, OldTime } = lists:keyfind(Name, 1, T),
	lists:keyreplace(Name, 1, T, { Name, OldTime+1 }).

merge(Ti, Tj) ->
	ZipFunction = fun({N, T1}, {N, T2}) -> { N, max(T1, T2) } end,
	lists:zipwith(ZipFunction, Ti, Tj).

leq(Ti, Tj) ->
	PredFunc = 
		fun({ N, T1 }) ->
			{_, T2} = lists:keyfind(N, 1, Tj),
			T1 =< T2
		end, 
	lists:all(PredFunc, Ti).

% retorna un reloj que pueda llevar cuenta de los nodos
clock(Nodes) ->
    lists:map(fun(N) -> { N, zero(Nodes) } end, Nodes).

% retorna un reloj que haya sido actualizado
% dado que hemos recibido un mensaje de log de un nodo en determinado
% momento.
update(Node, Time, Clock) ->
	case lists:keymember(Node, 1, Clock) of
		true  -> lists:keyreplace(Node, 1, Clock, { Node, Time });
		false -> [ { Node, Time } | Clock ]
	end.

% retorna true o false si es seguro enviar el mensaje
% de log de un evento que ocurrió en el tiempo Time dado.
safe(Time, Clock) -> 
    lists:all(fun({ _Node, T }) -> leq(Time, T) end, Clock).

%%%%% NUMEROS %%%%%

% zero() ->
%     0.

% inc(Name, T) ->
%     T+1.

% merge(Ti, Tj) ->
%     max(Ti, Tj).

% leq(Ti, Tj) ->
%     Ti =< Tj.
