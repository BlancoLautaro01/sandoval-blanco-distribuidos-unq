-module(worker).
-export([start/5, stop/1, peers/3]).

start(Name, Logger, Seed, Sleep, Jitter) ->
    spawn_link(fun() -> init(Name, Logger, Seed, Sleep, Jitter) end).

stop(Worker) ->
    Worker ! stop.

init(Name, Log, Seed, Sleep, Jitter) ->
    rand:seed(default, Seed),
    receive
        {peers, Nodes, Peers} ->
            loop(Name, Log, Peers, Sleep, Jitter, time:zero(Nodes));
        stop ->
            ok
    end.

peers(Wrk, Nodes, Peers) ->
    Wrk ! {peers, Nodes, Peers}.

loop(Name, Log, Peers, Sleep, Jitter, LamportTime) ->
    Wait = rand:uniform(Sleep),
    receive
        {msg, Time, Msg} ->
            ActualTime = time:inc(self, time:merge(Time, LamportTime)),
            Log ! {log, Name, ActualTime, {received, Msg}},
            loop(Name, Log, Peers, Sleep, Jitter, ActualTime);
        stop ->
            ok;
        Error ->
            Log ! {log, Name, time, {error, Error}}
    after Wait ->
        Selected = select(Peers),
        Message = {hello, rand:uniform(100)},
        Selected ! {msg, LamportTime, Message},
        jitter(Jitter),
        Log ! {log, Name, LamportTime, {sending, Message}},
        loop(Name, Log, Peers, Sleep, Jitter, time:inc(Name, LamportTime))
    end.

select(Peers) ->
    lists:nth(rand:uniform(length(Peers)), Peers).

jitter(0) -> ok;
jitter(Jitter) -> timer:sleep(rand:uniform(Jitter)).
