-module(test).
-export([run/2]).

%report on your initial observations
run(Sleep, Jitter) ->
    Nodes = [john, paul, ringo, george],
    Log = logger:start(Nodes),
    A = worker:start(john, Log, 13, Sleep, Jitter),
    B = worker:start(paul, Log, 23, Sleep, Jitter),
    C = worker:start(ringo, Log, 36, Sleep, Jitter),
    D = worker:start(george, Log, 49, Sleep, Jitter),
    worker:peers(A, Nodes, [B, C, D]),
    worker:peers(B, Nodes, [A, C, D]),
    worker:peers(C, Nodes, [A, B, D]),
    worker:peers(D, Nodes, [A, B, C]),
    timer:sleep(5000),
    logger:stop(Log),
    worker:stop(A),
    worker:stop(B),
    worker:stop(C),
    worker:stop(D).

