-module(logger).
-export([start/1, stop/1]).

start(Nodes) ->
    Clock = time:clock(Nodes),
    spawn_link(fun() -> init(Clock) end).

stop(Logger) ->
    Logger ! stop.

init(Clock) ->
    loop(Clock, queue:new()).

loop(Clock, Queue) ->
    receive
        {log, From, Time, Msg} ->
            UpdatedClock = time:update(From, Time, Clock),
            UpdatedQueue = case time:safe(Time, UpdatedClock) of
                    true ->
                        log(From, Time, Msg),
                        printSafeMsgs(Time, Queue),
                        notSafeMsgs(Time, Queue);
                    false -> 
                        queue:in({ From, Time, Msg }, Queue)
                end,
            loop(UpdatedClock, UpdatedQueue);
        stop ->
            ok
    end.

printSafeMsgs(Time, Queue) ->
    FilterFunction = fun({ _From, T, _Msg }) -> time:leq(T, Time) end,
    SafeMsgs = queue:filter(FilterFunction, Queue),
    printQueueMsgs(SafeMsgs).

printQueueMsgs(Queue) ->
    SortFunction = fun({_From, T1, _Msg}, {__From, T2, __Msg}) -> time:leq(T1, T2) end,
    SortedMsgs = lists:sort(SortFunction, queue:to_list(Queue)),
    lists:foreach(
        fun({ From, T, Msg }) -> 
            log(From, T, Msg) 
        end,
        SortedMsgs).

notSafeMsgs(Time, Queue) ->
    queue:filter(fun({ _From, T, _Msg }) -> not time:leq(T, Time) end, Queue).

log(From, Time, Msg) ->
    io:format("log: ~w ~w ~p~n", [Time, From, Msg]).
