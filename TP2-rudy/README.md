# Informe: 04-rudy

### Test

- ¿Cuántos requests por segundo podemos servir?
  En una primera implementación, al correr el benchmark logramos correr 100 requests en 4.3s, lo que se traduce en servir 23 requests por segundo.

- ¿Nuestro delay artificial es importante o desaparece dentro del overhead de parsing?
  Dado los resultados obtenidos en un server sin concurrencia y 40ms de delay como simulacion de procesamiento por cada request, podemos decir que el delay que le pusimos juega un papel importante ya que por cada request hay que esperar a que termine para que el server atienda la siguiente. Este problema escala por cada cliente que tengamos, ya que el server sigue siendo el mismo y sigue sin soportar concurrencia de requests.
  Al testear quitando ese delay de tan solo 40ms, se logra que nuestro servidor atienda las 100 peticiones en 0.21s y 1000 peticiones en 1.68s, por lo que dejamos evidenciado que aun con el overhead del parsing este delay empeora notablemente al tiempo de respuesta del servidor.

- ¿Qué ocurre si ejecutamos los benchmarks en varias máquinas al mismo tiempo?
  Con el delay artificial y sin concurrencia, nuestro servidor al pegarle con dos maquinas a la vez el benchmark, vemos como pasa a atender las cien requests en, por un lado 8.95s y por el otro 9,05s. Lo que pasó es que ahora, a las peticiones de una maquina, además del delay artificial de su request se le suma el tiempo de espera por atender otro request de la otra máquina. Quitando el delay, el servidor puede atender 2000 peticiones (1000 de cada maquina) en un total de 2.9s.

## Un poco más allá

### Incrementando el rendimiento

Así como está nuestro server, esperará por un request, la responderá, y esperará al siguiente. Si para servir el request dependemos de otros procesos, como un sistema de archivos o alguna base de datos, el server estará ocioso mientras otro request puede estar listo para ser parseado.

- ¿Deberíamos crear un nuevo proceso por cada request de entrada?

Sí, si al menos queremos aprovechar la concurrencia y que nuestro servidor sea mas veloz en tiempos de respuesta. La idea de procesar concurrentemente las request está destinada a aprovechar los "tiempos muertos" y no encolar request que podrían estar siendo procesadas.

- ¿Toma tiempo crear un nuevo proceso?

La creación de procesos en Erlang no es costosa, por lo que es una buena señal de ir por este camino si queremos aumentar el rendimiento del servidor.

- ¿Qué ocurriría si tenemos miles de requests por minuto?

Si tenemos miles de request por minuto, estaríamos generando miles de threads por minuto para atender a todos los clientes. Esto generaría que, el servidor pueda comenzar a volverse lento por cuestiones físicas(hardware). Ya que podríamos crear infinitos procesos, pero los procesadores tienen una cantidad finita de núcleos e hilos. Por otra parte, implementar un pool de handlers finito nos permitiría asegurar recursos restringiendo la cantidad de procesos a crear para cada request. De esta forma,en caso de recibir miles de request, podemos crear procesos finitos para maximizar el uso de los recusos del servidor y no crear miles de procesos, sino que tener N procesos para miles de requests.

### Parseo de HTTP

- ¿Cómo sabremos el tamaño del cuerpo de una request que llega dividida en varios bloques?

El tamaño del cuerpo de la request se determina por Content-Length, que es un campo que viene dentro del header.
Por ejemplo podríamos recibir una request con la siguiente información:

```
"GET /api/hola HTTP/1.1\r\nAccept: Content-Length: 16\r\n\r\nHola como estas?"
```

De esta manera, el tamaño del cuerpo lo conseguiríamos haciendo PM sobre el header, buscando el campo Content-Length y obteniendo su valor. Y por mas que llegue en partes, si obtenemos solo 5 caracteres podemos saber que el body estará incompleto hasta obtener los 16 caracteres del mismo.

#### Devolver archivos

Para devolver archivos debemos tenerlos dentro de nuestro proyecto y agregamos un caso de URI para que el cliente pueda solicitar el archivo. Luego, utilizando el modulo `file` con la función `read_file`, le pasamos el path absoluto del proyecto en donde queremos que busque dicho archivo, chequeamos los casos y retornamos el binario dentro del cuerpo en http

```erlang
find_file(URI, Ver) ->
    case file:read_file("./src/public" ++ URI) of
        {ok, Binary} -> http:ok(Ver, Binary);
        {error, enoent} -> http:ok(Ver, "[NOT_FOUND] foto no disponible");
        {error, _} -> http:ok(Ver, "[INTERNAL_ERROR]")
    end.
```

#### Robustez

La manera en que el server se apaga es, evidentemente, no muy elegante. Uno podría hacer un trabajo mucho mejor teniendo los sockets activos y enviar las conexiones como mensajes. El server podria entonces estar libre para recibir mensajes de control desde un proceso controlador o mensajes desde el socket.
Otro problema con la implementación actual, es que dejara el socket abierto si las cosas salen mal. Un mecanismo más robusto debe capturar excepciones y cerrar sockets, archivos abiertos, etc. antes de terminar.

Se implemento envios de mensajes a rudy para poder handlear tanto requests como delegar la responsabilidad de apagarse (kill al proceso). Este al apagarse tambien mata todos los subprocesos que esten handleando requests.

```erlang
% init rudy
case gen_tcp:listen(Port, Opt) of
    {ok, Listen} ->
        R = spawn(fun() -> request_receive(Listen) end),
        register(handler, R),
        receive
            kill ->
                io:format("[KILLING] Rudy web server :(~n"),
                handler ! kill,
                kill
        end,
        gen_tcp:close(Listen),
        ok;

...
% request_receive:
CONN = gen_tcp:accept(Listen, 1000),
    case CONN of
        {ok, Client} ->
            spawn(fun() -> request(Client) end),
            request_receive(Listen);
        {error, etimedout} ->
            receive
                kill ->
                    kill
            after 0 ->
                request_receive(Listen)
            end;
```

## Mejorando el benchmark

Buscamos la forma de hacer al benchmark dado un poco más realista. Entonces lo que hicimos fue, spawnear procesos para cada request de las N recibidas. Después lo que se hizo fue tener un proceso controlador para ir contando de 0 a N y, a la par, que el proceso de bench quede lockeado esperando a que le digan que termine. Cada vez que una request termina, le envía un mensaje al proceso controlador para notificarle que terminó, y cuando el controlador recibe los N avisos, le envía el mensaje de terminar al bench y allí se obtiene un resultado mucho más fiel a la concurrencia que tenemos en Rudy.
