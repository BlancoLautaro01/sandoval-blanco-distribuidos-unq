-module(test).
-export([bench/3]).

bench(N, Host, Port) ->
    Start = erlang:system_time(micro_seconds),
    B = spawn(fun() -> run(N, Host, Port, Start) end),
    register(benchmark, B),
    C = spawn(fun() -> controller(N) end),
    register(controller, C),
    io:format("[BENCHMARK] Ejecutando benchmark...~n").

controller(N) ->
    case N of
        0 ->
            io:format("[CONTROLADOR] Diciendole a benchmark que pare~n",[]),
            benchmark ! finish;
        N ->
            receive
                _ ->
                    controller(N-1)
            end
    end.

run(N, Host, Port, Start) ->
    if
        N == 0 ->
            wait_end(),
            Finish = erlang:system_time(micro_seconds),
            io:format("[BENCHMARK][FINALIZADO] Total en segundos: ~w~n",[(Finish - Start) / 1000000]), 
            ok;
        true ->
            spawn(fun() -> request(Host, Port) end),
            run(N-1, Host, Port, Start)
    end.

request(Host, Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    {ok, Server} = gen_tcp:connect(Host, Port, Opt),
    gen_tcp:send(Server, http:get("/api/hola")),
    Recv = gen_tcp:recv(Server, 0),
    case Recv of
        {ok, _} ->
            ok;
        {error, Error} ->
            io:format("test: error: ~w~n", [Error])
    end,
    gen_tcp:close(Server),
    controller ! ok.

wait_end() ->
    receive
        finish ->
            ok
    end.