-module(rudy).

-export([init/1]).

init(Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    case gen_tcp:listen(Port, Opt) of
        {ok, Listen} ->
            R = spawn(fun() -> request_receive(Listen) end),
            register(handler, R),
            receive
                kill ->
                    io:format("[KILLING] Rudy web server :(~n"),
                    handler ! kill,
                    kill
            end,
            gen_tcp:close(Listen),
            ok;
        {error, _} ->
            conection_error
    end.

request_receive(Listen) ->
    CONN = gen_tcp:accept(Listen, 1000),
    case CONN of
        {ok, Client} ->
            spawn(fun() -> request(Client) end),
            request_receive(Listen);
        {error, etimedout} ->
            receive
                kill ->
                    kill
            after 0 ->
                request_receive(Listen)
            end;
        {error, _} ->
            handler_error,
            request_receive(Listen)
    end.

request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
    case Recv of
        {ok, Str} ->
            Request = http:parse_request(Str),
            Response = reply(Request),
            gen_tcp:send(Client, Response);
        {error, Error} ->
            io:format("rudy: error: ~w~n", [Error])
    end,
    gen_tcp:close(Client).

reply({{get, URI, Ver}, _, _}) ->
    timer:sleep(40),
    find_resource(URI, Ver).

find_resource(URI, Ver) ->
    case URI of
        "/api/hola" ->
            http:ok(Ver, "[OK] mensaje de saludo: Hola!");
        "/api/chau" ->
            http:ok(Ver, "[OK] mensaje de despedida: Chau chau!");
        "/api/index.html" ->
            find_file("/index.html", Ver);
        _ ->
            http:ok(Ver, "[NOT_FOUND] :(")
    end.

find_file(URI, Ver) ->
    case file:read_file("./src/public" ++ URI) of
        {ok, Binary} -> http:ok(Ver, Binary);
        {error, enoent} -> http:ok(Ver, "[NOT_FOUND] foto no disponible");
        {error, _} -> http:ok(Ver, "[INTERNAL_ERROR]")
    end.
