-module(testMap).
-export([run/0]).

run() ->
    Map = map:new(),
    io:format("log: ~w~n", [Map]),
    Map2 = map:update(berlin, [quilmes, paris], Map),
    io:format("log: ~w~n", [Map2]),
    Map3 = map:update(paris, [berlin], Map2),
    io:format("log: ~w~n", [Map3]),
    Map4 = map:update(berlin, [paris], Map3),
    io:format("log: ~w~n", [Map4]),
    io:format("log: ~w~n", [map:all_nodes(Map4)]),
    io:format("log: ~w~n", [map:reachable(berlin, Map4)]).
