-module(test).
-export([run1/1, run2/1, run3/1]).

run1(Sleep) ->
    routy:start(r1, argentina),
    routy:start(r2, brazil),
    r1 ! {add, brazil, r2},
    r2 ! {add, argentina, r1},

    timer:sleep(Sleep),

    r1 ! {status, self()},
    receiveMsg(),

    r2 ! {status, self()},
    receiveMsg(),

    r1 ! stop,

    timer:sleep(Sleep),

    r2 ! {status, self()},
    receiveMsg(),
    r2 ! stop.

run2(Sleep) ->
    Routers = setup_arg(Sleep),

    status_argentina(Routers, Sleep),

    teardown_arg(Routers).

run3(Sleep) ->
    Routers = setup_arg(Sleep),

    sendMsg(
        Routers,
        Sleep,
        rosario,
        cordoba,
        "Envio desde rosario para cordoba"
    ),

    sendMsg(
        Routers,
        Sleep,
        cordoba,
        rosario,
        "Envio desde cordoba para rosario"
    ),

    sendMsg(
        Routers,
        Sleep,
        ushuaia,
        rosario,
        "Envio desde ushuaia para rosario"
    ),

    sendMsg(
        Routers,
        Sleep,
        tucuman,
        ushuaia,
        "Hola ushuaia"
    ),

    sendMsg(
        Routers,
        Sleep,
        bsas,
        bsas,
        "Hola bs as (yo mismo)"
    ),

    % no existe jujuy
    sendMsg(
        Routers,
        Sleep,
        bsas,
        jujuy,
        "Hola Jujuy"
    ),

    teardown_arg(Routers).

setup_arg(Sleep) ->
    Routers = [
        {r1, bsas},
        {r2, cordoba},
        {r3, rosario},
        {r4, tucuman},
        {r5, ushuaia}
    ],

    lists:foreach(
        fun({R, N}) ->
            routy:start(R, N)
        end,
        Routers
    ),

    % buenos aires
    r1 ! {add, cordoba, r2},
    r1 ! {add, rosario, r3},

    % cordoba
    r2 ! {add, bsas, r1},

    % rosario
    r3 ! {add, bsas, r1},
    r3 ! {add, tucuman, r4},

    % tucuman
    r4 ! {add, rosario, r3},
    r4 ! {add, ushuaia, r5},

    % ushuaia
    r5 ! {add, cordoba, r2},

    timer:sleep(Sleep),

    lists:foreach(
        fun({R, _}) ->
            R ! broadcast
        end,
        Routers
    ),

    timer:sleep(Sleep),

    lists:foreach(
        fun({R, _}) ->
            R ! update
        end,
        Routers
    ),

    timer:sleep(Sleep),

    Routers.

status_argentina(Routers, Sleep) ->
    lists:foreach(
        fun({R, _}) ->
            R ! {status, self()},
            receiveMsg()
        end,
        Routers
    ),
    timer:sleep(Sleep).

teardown_arg(Routers) ->
    lists:foreach(
        fun({R, _}) ->
            R ! stop
        end,
        Routers
    ),
    stopped.

sendMsg(Routers, Sleep, From, To, Msg) ->
    {R, _} = lists:keyfind(From, 2, Routers),
    io:format("------------------------------~n"),
    R ! {send, To, Msg},
    timer:sleep(Sleep).

receiveMsg() ->
    receive
        {status, {Name, N, Hist, Intf, Table, Map}} ->
            error_logger:info_msg(
                "[Router ~p]~n Counter ~p~n Hist ~p~n Intf ~p~n Table ~p~n Map ~p~n",
                [Name, N, Hist, Intf, Table, Map]
            )
    end.
