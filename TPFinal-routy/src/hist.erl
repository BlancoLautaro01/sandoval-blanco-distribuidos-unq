-module(hist).
-export([new/1, update/3]).

new(Name) -> [{Name, -1}].

update(Node, N, History) ->
    case lists:keyfind(Node, 1, History) of
        false ->
            {new, [{Node, N} | History]};
        {_, N2} ->
            case N2 < N of
                true -> {new, lists:keyreplace(Node, 1, History, {Node, N})};
                false -> old
            end
    end.
