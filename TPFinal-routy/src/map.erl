-module(map).
-export([new/0, update/3, reachable/2, all_nodes/1]).

new() -> [].

update(Node, Links, Map) ->
    lists:keystore(Node, 1, Map, {Node, Links}).

reachable(Node, Map) ->
    case lists:keyfind(Node, 1, Map) of
        {_, Links} -> Links;
        false -> []
    end.

all_nodes(Map) ->
    lists:usort(lists:flatmap(fun({N, LS}) -> [N | LS] end, Map)).
