-module(dijkstra).
-export([table/2, route/2]).

table(Gateways, Map) ->
    SortedMapNodes = [{X, inf, unknown} || X <- map:all_nodes(Map)],
    List = lists:foldl(
        fun({X, 0, X}, R) ->
            lists:keystore(X, 1, R, {X, 0, X})
        end,
        SortedMapNodes,
        [{X, 0, X} || X <- Gateways]
    ),
    SortedList = lists:keysort(2, List),
    iterate(SortedList, Map, []).

route(Node, Table) ->
    case lists:keyfind(Node, 1, Table) of
        false -> notfound;
        {_, Gateway} -> {ok, Gateway}
    end.

iterate(Sorted, Map, Table) ->
    case Sorted of
        [] ->
            Table;
        [{_, inf, unknown} | _] ->
            Table;
        [{Node, N, Gateway} | SL] ->
            RCH = map:reachable(Node, Map),
            NewSL = lists:foldl(fun(X, R) -> update(X, N + 1, Gateway, R) end, SL, RCH),
            iterate(NewSL, Map, lists:keystore(Node, 1, Table, {Node, Gateway}))
    end.

update(Node, N, Gateway, Sorted) ->
    case entry(Node, Sorted) of
        0 -> Sorted;
        N2 when N >= N2 -> Sorted;
        N2 when N < N2 -> replace(Node, N, Gateway, Sorted)
    end.

entry(Node, Sorted) ->
    case lists:keyfind(Node, 1, Sorted) of
        false -> 0;
        {_, N, _} -> N
    end.

replace(Node, N, Gateway, Sorted) ->
    lists:keysort(2, lists:keyreplace(Node, 1, Sorted, {Node, N, Gateway})).
