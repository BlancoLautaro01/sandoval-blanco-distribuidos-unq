# TP Final: Routy, un protocolo de ruteo link-state
- [Link a las diapositivas de la presentación](https://docs.google.com/presentation/d/17UbLCWeLF4q5n-DaSv9TTAAhurcNI0xJXWBOJXglric/edit?usp=sharing)
## Introducción

En esta actividad implementamos el protocolo de ruteo link-state, que es usado,
por ejemplo, para el protocolo OSPF (Open Shortest Path First, o Camino Más Corto Primero),
el cual utilizan routers de Internet cuyo objetivo principal es encontrar la ruta
más corta para el envío de información entre un router y otro, ubicados en diferentes lugares.

## Protocolo Link-state

En este protocolo cada nodo (router) construye su propio mapa de conectividad
dentro de la red, es decir, de cada nodo qué nodo son accesibles desde este.
En otras palabras, se realiza en base al grafo de la red, que indica qué nodos
están conectados.

Con ese mapa lo que hace cada nodo es calcular el mejor camino a cada nodo destino
dentro de esa red. Esa información se conoce como *tabla de ruteo*.

Por otra parte, la información de conectividad dentro de la red es compartida
entre los nodos de dicha red, para que todos posean la misma información, y así
poder calcular los caminos.

Se utilizan nombres lógicos para designar a los distintos nodos de la red.

### Información de los routers

Cada router dentro de la red guardará la siguiente información:

- Un **nombre simbólico** (ejemplo: buenos aires)

- Un **contador** (se explicará más adelante)

- Una estructura de datos que guarda la **historia de los mensajes recibidos**

- Un **conjunto de interfaces** (gateways), que son los nodos a los que está
directamente conectado

- Una **tabla de ruteo**, que indica a que gateway debe mandar un mensaje, 
según el destino de este

- Un **mapa de la red**, que compartirá con los demás nodos

Para las estructuras de datos (interfaces, tabla de ruteo, mapa de la red, historia de los mensajes) 
se define un módulo distinto.

### El mapa de la red

El módulo `map` encapsula el comportamiento sobre el mapa de la red. Es una
estructura de datos que consiste de una lista de pares `{ Nodo, Enlaces }`,
donde esos `Enlaces` son los nodos a los que puede acceder ese `Nodo`. 

Entonces, su interfaz nos permite saber a qué nodo está conectado cierto nodo,
y todos los nodos que hay en una red, más operaciones de creación y actualización
de dicho mapa.

La implementación fue trivial, dado que utilizamos operaciones como `lists:keystore`,
`lists:keyfind`, `lists:flatmap` y `lists:usort`, que nos permitieron implementar
cómodamente la interfaz pedida.

### Algoritmo de Dijkstra (construcción de la tabla de ruteo)

El cómputo de la tabla de ruteo se realiza a través del **algoritmo de Dijkstra**.

Este algoritmo nos permite, en general, encontrar el camino más corto entre todos los nodos
de un grafo, y por eso sirve para una red de nodos interconectados como routers sobre una red
de computadoras. Existen muchas variantes de este algoritmo. Este gráfico permite ilustrar cómo funciona en general:

<img src="https://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif">

Existe una variante que utiliza una cola de prioridad, pero en nuestro caso nos
alcanza con mantener una lista ordenada de menor a mayor, donde cada elemento
es una terna { `Nodo`, `N`, `Gateway` }, donde `N` es la cantidad de nodos
que hay entre un punto y otro de la red. Esa lista está ordenada entonces por
cantidad de saltos.

Como dijimos antes, este algoritmo se computa para cada nodo dentro de la red. 
Comienza recibiendo el mapa de la red y el conjunto de gateways. Lo que se
hace es agregar primero a la lista los nodos de los gateways, dado que su conexión
al nodo que computa la tabla es directa. Y luego se agregan todos los nodos
restantes del mapa, en principio con longitud infinita de saltos, lo cual
permite luego reemplazar eso por un número concreto, si es que se calcula.

Es un algoritmo recursivo, se va recorriendo la lista, hasta que se vacía o 
encontramos un nodo que da infinitos pasos. Mientras tanto lo que se hace con cada
elemento es consultar en el mapa para saber los nodos que son accesibles a través de
ese elemento actual. Dichos nodos actualizan la lista ordenada, poniendo como
cantidad de pasos, la cantida de pasos actual más uno, dado que se estaría generando
un saltó más. No obstante, ese dato reemplaza al actual sólo si es mejor, y por ejemplo,
para el caso de los nodos del conjunto de interfaces, eso no sucede nunca, dado que
existe conexión directa con los mismos. Cuando el algoritmo termina nos da
la tabla de ruteo para cada nodo destino en un mensaje se indica a qué nodo
con conexión directa nos conviene mandar ese mensaje, siendo ese el camino más
corto para mandarlo.

El nodo que computa la tabla también aparece en esa información, pero lo que se
hace al routear el mensaje es primero preguntar si el mensaje es para el router
que lo está recibiendo, en cuyo caso no se revisa la tabla de ruteo en cuestión.

Para implementar el algoritmo de Dijkstra, se hace uso de otras funciones que también trae Erlang.
En un principio cometimos algunos errores, como el de repetir nodos dentro de la tabla. Pero 
luego de varias pruebas pudimos dar con una implementación efectiva.

## Interfaces

Las interfaces simplemente son un conjunto de elementos no repetidos, de la
forma `{ Nombre, Ref, Pid }`, es decir, un nombre simbólico, una referencia
a un proceso, y un identificador de proceso.

La interfaz de este módulo nos permite agregar, borrar, y buscar información
por alguno de los campos antes dichos. También permite mandar un mensaje a
todos los nodos de la estructura. Esto se usa sobre todo para compartir
la información del mapa de la red.

## Historia para los mensajes

Llevar la historia de los mensajes permite detectar loops dentro de la red, es decir,
caminos cíclicos. Sin esto, un mensaje podría quedar dando vueltas en la red.

En el texto explica que podríamos llevar un contador que decrementamos en cada salto,
esperando que ese número alcance para que el mensaje llegue a su destino. Pero finalmente
propone simplemente almacenar los mensajes vistos hasta el momento, aunque en lugar
de guardar todo el mensaje, lo que guarda es un número. Entonces por ejemplo,
podemos saber si vimos ya el mensaje *17* del router *buenos aires*.

## Routers

El router, como siempre, es un proceso que se vale de los módulos antes descritos
para llevar a cabo su propósito de conducir mensajes a distintos destinos, que debe conocer,
en la red. Si no conoce el destino simplemente descarta ese mensaje.

En resumen, cada router va a:

- determinar con qué nodos está conectados

- comunicar a los nodos vecinos información de la red (el mapa)

- recibir mensajes que si no fueron vistos hasta el momento envía al nodo
que sabe que conduce por el camino más corto al destino de dicho mensaje

### Gateways y Desconexión de nodos

El loop principal del proceso queda a la espera de mensajes. Un conjunto de
esos mensajes permite agregar y borrar gateways. Cuando un gateway se agrega
al conjunto de interfaces, lo que se hace es asignarle un monitor de Erlang,
que nos avisará si la comunicación con ese nodo se cae por alguna razón. Si eso
sucede, estaremos a la espera de un mensaje `Down`, del monitor, y lo sacaremos
del conjunto.

### Status de la información

Un mensaje de status nos permite saber el estado interno del router en cualquier
momento. Esto nos sirvió muchísimo para validar que efectivamente los algortimos
estaban funcionando correctamente.

### Mensajes de link-state y construcción de la tabla de ruteo

Lo siguiente fue la implementación dentro del loop de los mensajes link-state.
Cuando se recibe un mensaje de este tipo, que contiene un contador, sobre el cuál
los siguientes tendrán un número mayor, lo que se
hace es chequear si es viejo o nuevo. Si es nuevo se actualiza el mapa de la red, lo
que significa que debemos correr el algoritmo de Dijkstra.

Pero para hacer las cosas más granulares, el aviso de la nueva información
a los nodos vecinos y el nuevo cómputo de la tabla de ruteo se realizan en otros
dos mensajes de `broadcast` y `update` respectivamente.

El texto aclara que el algoritmo de Dijkstra debería invocarse periódicamente,
incluso sobre todo cada vez que recibimos un mensaje link-state, aunque lo hacemos
cada vez que el mapa se actualiza.

## Escenarios de Prueba

Lo que primero nos dimos cuenta es que cada vez que modificamos el mapa de la red,
los nodos demoraban un poco en actualizar su información, pero sobre todo
en recomputar su tabla de ruteo. Por eso agregamos algunos sleep, que nos
permitieron testear más cómodamente el escenario.

Hicimos tres pruebas. 

Una prueba simplemente la conexión entre dos routers, y lo hicimos sobre
todo para testear bien los casos en los que uno se caía. Sucedió lo mismo
que en otra de las actividades que hicimos con Erlang para esta materia. Si el cable
lo desconectábamos por mucho tiempo tiraba algunos errores, aunque luego volvía a conectarse
correctamente. Pero en algunas ocasiones lo que sucedió fue que el nodo desconectado
era eliminado como gateway, con lo cual quedaba inconexo de la red, y teníamos
que volver a agregarlo a mano.

Las otras dos pruebas consistieron en mapear toda una red con más nodos. 
Para testear mejor los casos anómalos, y en general tener un ejemplo con el 
cual probar el comportamiento de los routers sobre la red, modelamos 
un escenario de prueba con el siguiente esquema:

<img src="./ejemplo.jpg">

Entonces, la segunda prueba consistió en verificar que el algoritmo de Dijkstra ejecutaba
correctamente. Nos costó hacerlo funcionar por distintas razones, sobre todo
por errores nuestros dentro del algoritmo, o por malentendidos en relación al texto,
pero finalmente tuvimos éxito en ese aspecto. Pensamos que algunas partes podrían
mejorarse, sobre todo porque reordenamos la lista en varios lugares. Ciertamente
con una cola de prioridad el algoritmo debería mejorar.

La tercer prueba consistió en rutear una serie de mensajes por la red. Los enviós
de mensajes fueron los siguientes:

1) De `rosario` a `cordoba` ("Hola cordoba"). Entonces debería pasar por buenos aires, por ser el camino más corto.

```
<rosario>: routing from <rosario> to <cordoba> ("Hola cordoba")
<bsas>: routing from <rosario> to <cordoba> ("Hola cordoba")
<cordoba>: from <rosario> received ("Hola cordoba")
```

2) De `cordoba` a `rosario` ("Hola rosario"). Debía volver por el mismo lugar

```
<cordoba>: routing from <cordoba> to <rosario> ("Hola rosario")
<bsas>: routing from <cordoba> to <rosario> ("Hola rosario")
<rosario>: from <cordoba> received ("Hola rosario")
```

3) De `ushuaia` a `rosario` ("Hola tucuman")

```
<ushuaia>: routing from <ushuaia> to <rosario> ("Hola tucuman")
<cordoba>: routing from <ushuaia> to <rosario> ("Hola tucuman")
<bsas>: routing from <ushuaia> to <rosario> ("Hola tucuman")
<rosario>: from <ushuaia> received ("Hola tucuman")
```

4) De `tucuman` a `ushuaia` ("Hola ushuaia")

```
<tucuman>: routing from <tucuman> to <ushuaia> ("Hola ushuaia")
<ushuaia>: from <tucuman> received ("Hola ushuaia")
```

5) De `bsas` a `bsas` ("Hola bs as"). Este no debía rutearse, sino quedarse
en `bsas`.

```
<bsas>: from <bsas> received ("Hola bs as (yo mismo)")
```

6) De `bsas` a `jujuy`. Como no podía llegar a `jujuy` (porque no existe dentro de la red) el mensaje se pierde.

```
<bsas>: routing from <bsas> to <jujuy> ("Hola Jujuy")

=INFO REPORT==== 25-May-2016::21:59:11 ===
bsas route jujuy not found
```

## Conclusiones

Implementamos el protocolo link-state que nos permitió construir por un lado
tablas de ruteo con el camino más corto para cada nodo dentro de una red. Probamos
con distintos casos su funcionamiento, incluso desconectando algunos nodos para verificar
la consistencia de los datos.

Ciertamente fue un trabajo bastante desafiante a pesar de tener buena cantidad de código ayuda, 
dado que le dedicamos muchas horas a corregir los algoritmos hasta hacerlos funcionar
correctamente. Eso nos forzó a pensar en distintos casos borde que sacarán a la luz
los casos anómalos, donde por ejemplo, ordenábamos mal la información, o teníamos
información repetida donde no debía haberla.

Fue de gran ayuda leer cómo funcionan esos algoritmos en la vida real. Usamos
como fuente principal wikipedia, aunque también consultamos apuntes que teníamos
sobre Algoritmos de la Licenciatura en Desarrollo de Software (habíamos visto
ya el algoritmo de Dijkstra sobre grafos en general).

Como mejoras que fuimos pensando, una importante es utilizar una cola de prioridad
para mejorar los tiempos del algoritmo de Dijkstra.

Notamos que es importante que todos los nodos de la red tengan lo más actualizada
posible tanto su mapa de la red como su tabla de ruteo. Detectamos casos en 
donde es posible que se formen ciclos si varios nodos no están trabajando
con el mismo mapa (por ejemplo, un nodo posee como información que otro es el mejor camino
para enviar un mensaje, y viceversa). Llegamos a probar la red con 100 nodos interconectados,
y la información demoraba en actualizarse.

Concluimos que este fue uno de los ejemplos más clarificadores sobre el tema de
sistemas distruidos, tal vez por ser uno que pensamos siempre que hablamos de
redes de computadoras aplicado a algo que utilizamos todos los días como lo es el internet.
