-module(ping_sandoval).

-export([start/0, ping_listen/0, ping/1, kill/1, counter/1]).

start() ->
    Pid = spawn(ping_sandoval, ping_listen, []),
    CounterPid = spawn(ping_sandoval, counter, [0]),
    register(ping_listen_process, Pid),
    register(ping_counter_process, CounterPid).

ping_listen() -> 
    receive
        {ping, Pid} ->
            io:format("[RECIBIDO] PING DESDE: ~w.~n", [Pid]),
            ping_counter_process ! incrementar,
            Pid ! pong,
            ping_listen();
        {kill, Pid} ->
            io:format("[KILLED].~n", []),
            ping_counter_process ! kill,
            Pid ! killed,
            ok
    end
.

ping(Node) ->
    {ping_listen_process, Node} ! {ping, self()},
    receive
        pong ->
            io:format("[RECIBIDO] PONG.~n", [])
    after 2000 ->
        io:format("[NO_RECIBIDO] PONG :(~n", [])
    end.

kill(Node) ->
    {ping_listen_process, Node} ! {kill, self()},
    receive
        killed ->
            io:format("[KILLED].~n", [])
    after 2000 ->
        io:format("[NO_RECIBIDO] notificación.~n", [])
    end.

counter(Actual) ->                            
    receive                                   
        incrementar -> 
            counter(Actual + 1);
        kill ->
            io:format("[FIN] PINGS TOTALES: ~w.~n", [Actual])                       
    end.
